(function() {
   
    var MyAd = function() {
        this.is_sparta = true;
    }
    
    MyAd.prototype.build_ad = function() {
        if(this.is_sparta) console.log("This is Sparta!");
    }
    
    var ad = new MyAd();
    ad.build_ad();
    
})();
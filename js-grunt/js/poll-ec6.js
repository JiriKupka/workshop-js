class Poll {
    
    constructor(container_id, question, options) {
            
        this.container = document.getElementById(container_id);
        this.question = question;
        this.options = options;
        this.selected_option = 0;
        
        this.build_form();
        
    }
    
    
    build_form() {
        var form = document.createElement('form');
        var question = document.createElement('p');
        var options_container = document.createElement('div');
        var submit = document.createElement('input');
        
        submit.setAttribute('type','submit');
        submit.setAttribute('value','Hlasovat');
        
        for(let i = 0; i < this.options.length; i++) {
            
                var option_container = document.createElement('div');
                var option_input = document.createElement('input');
                var option_label = document.createTextNode(this.options[i]);
        
                option_input.setAttribute('type', 'radio');
                option_input.setAttribute('name', 'poll_option');
        
                option_input.addEventListener('click', () => {
                    this.selected_option = i;
                });
        
                option_container.appendChild(option_input);
                option_container.appendChild(option_label);
                options_container.appendChild(option_container);
        
        }
        
        submit.addEventListener('click',() => {
           alert("Děkuji, že jste si vybrali odpověď "+this.selected_option); 
        });
        
        form.appendChild(question);
        form.appendChild(options_container);
        form.appendChild(submit);    
        
        this.container.appendChild(form);
    }
    
}
    
window.addEventListener('load',function() {
    var poll = new Poll('poll',"Zrušili byste letní čas?",['ano', 'ne', 'nevím']);
});
var React = require('react');
var express = require('express');

var BaseTemplate = require('./public/views/base').BaseTemplate;

module.exports = function(app) {
	app.use('favicon.ico',express.static('public/static/img'));
	app.use(express.static('public'));

	app.get('/', function(req, res){
		var reactHtml = React.renderToString(<BaseTemplate title="Titulek úvodní stránky!" />);
		res.end(reactHtml);
	});

	app.get('/:article', function(req, res) {
		var reactHtml = React.renderToString(<BaseTemplate article={req.params.article} title="Detail článku" />);
		res.end(reactHtml);
	});

};
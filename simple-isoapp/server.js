require("node-jsx").install();

var express = require('express');
var router = require('./router');

var app = express();
router(app);

app.listen(process.env.PORT || 5000);
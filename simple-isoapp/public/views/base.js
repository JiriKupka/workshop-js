var React = require('react/addons');
var Controller = require('../controller');

var BaseTemplate = React.createClass({

	render: function() {

		var Navbar = require('./navbar').Navbar;
		var Content = require('./content').Content;

		var article = Controller.getArticleByName(this.props.article);

		return (
			<html>
				<head>
					<title>Isomorfní aplikace</title>
					<link href='/static/styles.css' rel="stylesheet" />
					<meta charSet="utf-8" />
				</head>
				<body>
					<Navbar />
					<div id="main-container" className="container">
						<div className="row">
							<Content article={article} />
						</div>
					</div>
					<script src="/static/universal.js"></script>
				</body>	
			</html>);
	}

});

module.exports.BaseTemplate = BaseTemplate;
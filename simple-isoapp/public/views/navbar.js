var React = require('react/addons');

var Navbar = React.createClass({

	render: function() {
		return (<div id="navbar">
			<nav className="navbar navbar-default">
				<div className="container">
					<div className="navbar-header">
						<a className="navbar-brand" href="/">Isomorfní aplikace</a>
					</div>

					<div className="collapse navbar-collapse">
						<ul className="nav navbar-nav">
							<li><a href="/clanek1">Článek 1</a></li>
							<li><a href="/clanek2">Článek 2</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>);
	}

});

module.exports.Navbar = Navbar;
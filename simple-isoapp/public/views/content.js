var React = require('react/addons');

var Content = React.createClass({

	render: function() {
		return (
				<div className="col-md-12">
					<h1>{this.props.article.title}</h1>
					<div dangerouslySetInnerHTML={{__html:this.props.article.content}} />
				</div>
			);
	}

});

module.exports.Content = Content;
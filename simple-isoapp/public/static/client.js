(function() {

	var Controller = require('../controller');
	var BaseTemplate = require('../views/base').BaseTemplate;
	var React = require('react/addons');

	var links = document.querySelectorAll('a');

	for(let i = 0; i < links.length; i++) {
		var link = links[i];

		link.addEventListener('click',function(e) {
			var path = this.pathname.substring(1);

			var reactHtml = React.renderToString(<BaseTemplate article={path} title="Detail článku" />);
			document.write(reactHtml);
			document.close();

			history.pushState({}, "Detail článku", path);

			e.preventDefault();
		});

	}

})();